import React from 'react'
import styled, { ThemeProvider } from 'styled-components'
import Text from "./Text";
import Button from "./Buttons"
import theme from '../lib/Theme';
import { Box } from './Grid';

const Boostbg = styled.div`
width : 100%;
height : 180px;
background :#3b3054;

@media (max-width : 400px){
height : 250px;

}
`
const BoostOverlay = styled.div`
display : flex;
flex-direction : column;
justify-content : center;
align-items : center;
width : 100%;
height : 100%;
background-image : url('/bg-boost-desktop.svg');
background-size : cover;
background-posotion : center;

@media(max-width : 400px){
    background-image : url('/bg-boost-mobile.svg');
    height : 180px;
}
`

export default function Boost() {
    return (
        <ThemeProvider theme ={theme}>
            <Box height = '180px' bg = 'darkViolet'>
           <BoostOverlay>
                <Text color = "#fff" fontWeight = "semi-bold" fontSize = {["big","larger"]} >Boost your links today</Text>
                <Button primary color = "white" bg = "#2acfcf" p = "10px 20px" mt= {[1]}>Get started</Button>
           </BoostOverlay> 
        </Box>
        </ThemeProvider>
    )
}
