import React from 'react'
import styled, { ThemeProvider } from "styled-components";
import Image from "./Image";
import Text from './Text'
import theme from '../lib/Theme';
import { space, typography } from 'styled-system';
import { Box, Flex } from './Grid';


const linksData = [
    {
        heading : 'Features',
        link : ['Link Shortening' , 'Branded Links' , 'Analytics']
    },
    {
        heading : 'Resources',
        link : ['blog' , 'Developers' , 'Support']
    },
    {
        heading : 'Company',
        link : ['About' , 'Our Team' , 'Careers', 'Contact']
    }
]



const icondet = ['/icon-facebook.svg', '/icon-twitter.svg', '/icon-pinterest.svg', '/icon-instagram.svg']

export default function Footer() {
    return (
        <ThemeProvider theme = {theme}>
            <Flex bg = 'darkBlue' flexDirection = {['column', 'row']}  justifyContent = 'space-between' alignItems = {['center', 'flex-start']} p = {[4]} px = {[9]}> 
            <Image src = '/logowhite.svg'/>
            <Flex textAlign = {['center', 'left']} flexDirection = {['column' , 'row']}>
                {linksData.map((item) => (
                    <Box mb = {['20px', '0']} mx = {['auto', 4]}>
                        <Text color = "white" fontWeight = "500" fontSize = {["20px","15px"]} mb = "10px" key = {item.heading}>{item.heading}</Text>
                        {item.link.map((link) => (
                            <Text fontSize = "11px" fontWeight = "500" color = "#bfbfbf" my = "7px" key = {link}>{link}</Text>
                        ))}
                    </Box>
                ))}
            </Flex>
            <Flex>
            {icondet.map((icon) => (
                <Image src = {icon} mx = "10px"/>
            ))} 
            </Flex>  
        </Flex>
        </ThemeProvider>
    )
}
