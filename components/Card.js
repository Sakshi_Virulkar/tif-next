import React from 'react'
import styled, { ThemeProvider } from "styled-components";
import { size, space } from "styled-system";
import Image from "./Image"
import Text from "./Text"
import theme from '../lib/Theme';
import { Box, Flex } from './Grid';

const Sticker = styled.div`
display : flex;
justify-content : center;
align-items : center;
background : #3b3054;
width : 70px;
height : 70px;
border-radius : 100px;
transform : translate(0 , -50%);

@media (max-width : 400px){
    margin : auto
}
`
const Card = ({name ,content , mark , margin}) => {
    return (
        <ThemeProvider theme = {theme}>
            <Flex flexDirection = 'column'bg = 'white'mt = {[5, margin]} width = {['100%', '350px']} height = {['220px', '250px']} pb = {[1]} px = {[3,4]}borderRadius = '5px' zIndex = '99'>
            <Sticker>
                <Image src = {mark} alt = "" width = "30px" ></Image>
            </Sticker>
            <Box>
            <Text color = "#35323e" fontWeight = "700" fontSize = {["title","big"]} >{name}</Text>
            <Text my = "20px" fontSize = {["11px","14px"]} color = "#9e9aa7" fontWeight = "500">{content}</Text>
            </Box>
        </Flex>
        </ThemeProvider>
    )
}


export default Card
