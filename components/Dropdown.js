import React from 'react'
import { ThemeProvider } from "styled-components";
import theme from '../lib/Theme';
import Button from './Buttons';
import { Flex } from './Grid';




export default function Dropdown({drop}) {



    return (
        <ThemeProvider theme = {theme}>
            <Flex display = {['flex', 'none']} flexDirection = 'column' bg = 'darkViolet' width = '80%' margin ='auto' p = {[1]} borderRadius = '5px'>
                <Button color = "white">Features</Button>
                <Button color = "white">pricing</Button>
                <Button color = "white">resources</Button>
                <hr style = {{margin : "20px", border : "1px solid #fff"}} />
                <Button color = "white">log in</Button>
                <Button color = "white">sign up</Button>
            </Flex>
        </ThemeProvider>
    )
}
