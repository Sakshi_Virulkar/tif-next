import React, {useState}  from 'react'
import {ThemeProvider} from 'styled-components'
import theme from '../lib/Theme';
import Button from './Buttons'
import Image from "./Image";
import Dropdown from './Dropdown';
import { Flex } from './Grid';





export default function Navbar() {
    const [drop, setDrop] = useState('false')


    return (
        <ThemeProvider theme = {theme}>
        <>
        <Flex px = {[3,8]} py = {[3,4]} justifyContent = 'space-between' alignItems= 'center' width = '100%'>
            <Flex>
            <Image src = '/logo.svg' alt = "" width = {['5rem' , '10rem']}   ></Image>
            <Flex display = {['none' , 'flex']} align-items = "center">
            <Button secondary ml = {[3]}>
                Features
            </Button>
            <Button secondary>
                pricing
            </Button>
            <Button secondary>
                resources
            </Button>
            </Flex>
            </Flex>

            <Flex display = {['none' , 'flex']}>

            <Button primary>
                log in
            </Button>
            <Button primary>
                sign up
            </Button>
            </Flex>
            <Button secondary  p = "0" display = {['block','none']} onClick = {() => (setDrop(!drop))}>
                <Image src = '/menu.svg' width = "20px" ></Image>
            </Button>
        </Flex>
        {drop && <Dropdown />}
        </>
        </ThemeProvider>
    )
}
