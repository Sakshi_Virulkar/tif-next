import styled from 'styled-components';
import {
  space,
  layout,
  display,
  color,
  flexbox,
  border,
  position,
  typography,
  shadow
} from 'styled-system';

export const Box = styled.div`
    ${color}
    ${layout}
    ${space}
    ${display}
    ${flexbox}
    ${position}
    ${typography}
    ${border}
    ${shadow}
`;

export const Flex = styled.div`
    display: flex;
    ${color}
    ${layout}
    ${space}
    ${display}
    ${flexbox}
    ${position}
    ${typography}
    ${border}
    ${shadow}
`;
