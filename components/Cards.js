import React from 'react'
import Card from "./Card";
import styled, { ThemeProvider } from "styled-components"
import theme from '../lib/Theme';
import { space } from 'styled-system';
import { Flex } from './Grid';

const Cardbox = styled.div`
position : relative;
display : flex;
justify-content : space-between;
width : 80vw;
margin-bottom : 100px;

@media (max-width : 400px){
    flex-direction : column;
}

${space}
`

const Line = styled.div`
position : absolute;
width : 100%;
height : 8px;
background :#2acfcf;

top : 50%;
z-index : 9;

@media (max-width : 400px){
    transform : rotate(90deg);
    width : 500px;
    right : -48%;

}
`

const cardData = [
    {
        name : "Brand Reacognition",
        content : "Boost your brand recognition with each click. Genric links don't mean a thing. Branded links instil confidence in your content",
        mark : '/icon-brand-recognition.svg',
        margin : "0px"
    },
    {
        name : "Detailed Records",
        content : "Gain insights into whois clicking your links. Knowing when and where people engage in your content helps inform better decisions",
        mark : '/icon-detailed-records.svg',
        margin : "30px"
    },
    {
        name : "Fully Customizable",
        content : "Improve brand awareness and content discoverability thorugh costumizable links, supercharging audience engagement.",
        mark : '/icon-fully-customizable.svg',
        margin : "60px"
    }
]


export default function Cards() {
    return (
        <ThemeProvider theme = {theme}>
        <Flex mt = {[3, 5]} mb = {[9]} width = '80%' flexDirection = {['column', 'row']} justifyContent = 'space-between' position = 'relative'>
        <Line />
        {cardData.map((item) => (
            <Card {...item} key = {item.name}/>
            ))}
        </Flex>
        </ThemeProvider>
    )
}
