import styled from "styled-components";
import { color, typography , space, layout} from "styled-system";

const Text = styled.p`
color : black;
display : block;
font-size: ${(props) => props.theme.fontSizes.body};
    letter-spacing: '.3px';


${color}
${typography}
${space}
${layout}


`

export default Text