import React from 'react'
import styled, { ThemeProvider } from "styled-components"
import Text from "./Text";
import Shortenlink from './Shortenlink'
import Cards from "./Cards";
import theme from '../lib/Theme';
import {Flex} from '../components/Grid'






export default function Lowerbox() {
    return (
        <ThemeProvider theme = {theme}>
            <Flex flexDirection = 'column' mt = {[9,8]} minHeight = {["100vh","80vh"]} bg = 'lightGray' alignItems = 'center'>
            <Shortenlink /> 
            <Text textAlign  = "center" color = "#9e9aa7" fontSize = {["13px","17px"]} p = {['20px', '0']} width = {['90%', '40%']}><Text fontSize = {["22px","35px"]} fontWeight = "600" color = "#35323e" mb = {['10px', '0']}>Advanced Statistics</Text>Test how your links are performing accross the web with our advanced statistics dashboard.</Text>
            <Cards />
        </Flex>
        </ThemeProvider>
    )
}
