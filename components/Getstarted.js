import React from 'react'
import Text from "../components/Text";
import Button from "./Buttons";
import Image from "./Image";
import {ThemeProvider} from 'styled-components'
import theme from '../lib/Theme';
import { useRouter } from 'next/router'
import { Box } from "./Grid";





export default function Getstarted() {
    const router = useRouter()

    const handleClick = (e) => {
        e.preventDefault()
        router.push('/shorten')
      }
    return (
        <ThemeProvider theme = {theme}>
            <Box px = {[1,8]} mt = {[4,1]} mb = {[1,11]} position = 'relative' width = '100%' >
            <Image src = '/illustration-working.svg' alt = "" width = {['90vw', '50vw']}  position = "absolute" right = {["5%","-20px"]} top  = {["-140%","0px"]} zIndex = "9"></Image>
            <Box mt = {[11,4]} textAlign = {['center', 'left']} width = {['100%', '45%']}>
            <Text fontSize = {['big','60px']} my= "-10px" fontWeight = "900" color = "#232127">More then just<br /> shorter links</Text>
                <Text my = {["15px","20px"]} color = "#9e9aa7" fontSize = {['13px' , '20px']} >Build your brand'sreacogintion and get detailed insights on how your links are performing</Text>
                <Button color = "white" bg = "#2acfcf" fontSize = {['title','big']} px = "30px" mt = {["10px","20px"]} onClick = {handleClick}>Get started</Button>
            </Box>
        </Box>
        </ThemeProvider>
    )
}
