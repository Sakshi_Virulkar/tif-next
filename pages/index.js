import React from 'react'
import Navbar from '../components/Nav'
import Getstarted from '../components/Getstarted'
import Boost from '../components/Boost';
import Footer from '../components/Footer';
export default function Home() {
  return (
    <div>
      <Navbar />
      <Getstarted />
      <Boost />
      <Footer /> 

    </div>
  )
}
