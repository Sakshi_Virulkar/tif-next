import React from 'react'
import Navbar from '../components/Nav'
import Lowerbox from "../components/Lowerbox";
import Boost from '../components/Boost';
import Footer from '../components/Footer';

export default function shorten() {
    return (
        <div>
        <Navbar />
        <Lowerbox />
        <Boost />
        <Footer /> 
        </div>
    )
}
