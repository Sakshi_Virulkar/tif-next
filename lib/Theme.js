const theme = {
    breakpoints : ['400px', '1440px'],
        fontSizes: {
          tiny: '.5rem',
          small: '.625rem',
          body: '.75rem',
          smtitle: '0.8625rem',
          subtitle: '.875rem',
          title: '1rem',
          big: '1.25rem',
          large: '2rem',
          larger: '2.5rem'
        },
        space: [
          '0',
          '0.5rem',
          '1rem',
          '2rem',
          '3rem',
          '4rem',
          '5rem',
          '6rem',
          '7rem',
          '8rem',
          '9rem',
          '15rem'
        ],
        colors: {
          primary: '#10967c',
          primaryDark: '#0E856D',
          primaryLight: '#03A87C',
          primaryBg: '#e6f3f0',
          secondary: '#48a97c',
          background: '#f8f8f8',
          light: '#aaa',
          grayer: '#142F47',
          white: '#ffffff',
          black: '#000000',
          gray: '#dddddd',
          danger: '#E17055',
          mute: '#8FA2AA',
          blueBg: '#E6F3F0',
          error: '#f46262',
          placeholder: '#c5c5c5',
          warning: '#ff9800',
          darkViolet : '#3b3054',
          darkBlue : '#35323e',
          lightGray : '#EFF1F7'
        }
      };
      


export default theme